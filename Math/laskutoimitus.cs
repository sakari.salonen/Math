﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math
{
    class Laskutoimitus
    {
        public int Add(int a, int b)    // Ynnäyslasku toiminto [Addition]
        {
            return a + b;
        }
        public int Sub(int a, int b)    // Vähennyslasku toiminto [Subtraction]
        {
            return a - b;
        }
        public int Div(int a, int b)    // Jakolasku toiminto [Division]
        {
            return a / b;
        }
        public int Multi(int a, int b)  // Kertolasku toiminto [Multiplication]
        {
            return a * b;
        }
    }
}
